import numpy as np
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt
import re

laptops = pd.read_csv("datasets/laptops.csv", encoding="utf-16")

laptops.head()

def extract_ram(x):
    match = re.search(r"(\d+)GB", x )
    if match:
        return match.group()
    else:
        raise Exception("Error al extreure la RAM", x)

laptops["Ram"] = laptops["Ram"].apply(extract_ram)
print(laptops["Ram"])

def extract_cpu(x):
    match = re.search(r"(\d*(\.\d+)?)GHz", x )
    if match:
        return match.group()
    else:
        raise Exception("Error al extreure el CPU", x)

laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
print(laptops["Cpu"])

def extract_resol(x):
    match = re.search(r"(\d+)x(\d+)", x )
    if match:
        return match.group()
    else:
        raise Exception("Error al extreure la resolucio", x)


laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_resol)
print(laptops["ScreenResolution"])



y = laptops["Price_euros"]


# Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
plt.plot(laptops["Ram"], y, "-", label='linear')  # Plot some data on the axes.
plt.xlabel('x label')  # Add an x-label to the axes.
plt.ylabel('y label')  # Add a y-label to the axes.
plt.title("Simple Plot")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.axhline(y=0, color='k')
plt.axvline(x=0, color='k')
plt.show()

